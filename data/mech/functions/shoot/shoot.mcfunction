# mech:shoot/shoot

execute if entity @s[nbt={SelectedItem:{tag:{type:0}}}] run function mech:shoot/pod
execute if entity @s[nbt={SelectedItem:{tag:{type:1}}}] run function mech:shoot/bon
execute if entity @s[nbt={SelectedItem:{tag:{type:2}}}] run function mech:shoot/far
execute if entity @s[nbt={SelectedItem:{tag:{type:3}}}] run function mech:shoot/mig
# execute if entity @s[nbt={SelectedItem:{tag:{type:4}}}] run function mech:shoot/lar
# execute if entity @s[nbt={SelectedItem:{tag:{type:5}}}] run function mech:shoot/par

execute store result score @s fireCooldown run data get entity @s SelectedItem.tag.FireCooldown

scoreboard players operation @e[tag=new] id = @s id
scoreboard players set @e[tag=new] prevSelect -1
tag @e[tag=new] add haveId

tag @e[tag=new] add bullet
tag @e remove new

title @s actionbar {"text" : "X", "color" : "red"}