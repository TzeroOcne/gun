# mech:hit/wall/reduce/step_2

# say step 2
# summon armor_stand ~ ~ ~ {NoGravity:1b}
# replaceitem entity @e[type=armor_stand,limit=1,sort=nearest] armor.head granite
execute unless block ~ ~ ~ #mech:translucent run tag @s add foundWall
execute if entity @s[tag=foundWall] if score #countX global matches 1 run tag @s add countX
execute if entity @s[tag=foundWall] if score #countY global matches 1 run tag @s add countY
execute if entity @s[tag=foundWall] if score #countZ global matches 1 run tag @s add countZ
# tellraw @a [{"nbt" : "Tags", "entity" : "@s"}]

execute if score #countX global matches 2 if entity @s[tag=!foundWall,y_rotation=-180..0] positioned ~1 ~ ~ run function mech:hit/wall/reduce/step_3
execute if score #countX global matches 2 if entity @s[tag=!foundWall,y_rotation=0..180] positioned ~-1 ~ ~ run function mech:hit/wall/reduce/step_3

execute if score #countY global matches 2 if entity @s[tag=!foundWall,x_rotation=-90..0] positioned ~ ~1 ~ run function mech:hit/wall/reduce/step_3
execute if score #countY global matches 2 if entity @s[tag=!foundWall,x_rotation=0..90] positioned ~ ~-1 ~ run function mech:hit/wall/reduce/step_3

execute if score #countZ global matches 2 if entity @s[tag=!foundWall,y_rotation=-90..90] positioned ~ ~ ~1 run function mech:hit/wall/reduce/step_3
execute if score #countZ global matches 2 if entity @s[tag=!foundWall,y_rotation=90..-90] positioned ~ ~ ~-1 run function mech:hit/wall/reduce/step_3