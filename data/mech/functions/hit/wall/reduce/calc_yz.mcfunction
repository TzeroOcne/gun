# mech:hit/wall/reduce/calc_yz

scoreboard players operation #dumbY global = #dltaY global
scoreboard players operation #dumbZ global = #dltaZ global

scoreboard players operation #dumbY global *= @s velcZ
scoreboard players operation #dumbZ global *= @s velcY

# execute if score #dumbY global > #dumbZ global run tag @s remove countZ
execute if score #dumbY global > #dumbZ global run scoreboard players add #countZ global 1
execute if score #dumbZ global > #dumbY global run scoreboard players add #countY global 1