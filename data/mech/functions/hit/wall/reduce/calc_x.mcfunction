# mech:hit/wall/reduce/calc_x

scoreboard players operation #midX global = #lastX global
scoreboard players operation #midX global += #currX global
scoreboard players operation #midX global *= #500 global
scoreboard players add #midX global 500
scoreboard players operation #dltaX global = #midX global
execute store result score #lastX global run data get entity @s Pos[0] 1000
scoreboard players operation #dltaX global -= #lastX global
# scoreboard players operation #moveX global -= #lastX global
# execute if score #moveX global matches ..0 run scoreboard players operation #moveX global *= #-1 global
execute if score #dltaX global matches ..0 run scoreboard players operation #dltaX global *= #-1 global
scoreboard players set #countX global 0