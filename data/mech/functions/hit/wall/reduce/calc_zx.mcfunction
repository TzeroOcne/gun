# mech:hit/wall/reduce/calc_zx

scoreboard players operation #dumbZ global = #dltaZ global
scoreboard players operation #dumbX global = #dltaX global
# tellraw @a [{"score" : {"name" : "#dumbZ", "objective" : "global"}}, {"text" : " "}, {"score" : {"name" : "#dumbX", "objective" : "global"}}]

scoreboard players operation #dumbZ global *= @s velcX
scoreboard players operation #dumbX global *= @s velcZ
# tellraw @a [{"score" : {"name" : "#dumbZ", "objective" : "global"}}, {"text" : " "}, {"score" : {"name" : "#dumbX", "objective" : "global"}}]

# execute if score #dumbZ global > #dumbX global run tag @s remove countX
execute if score #dumbZ global > #dumbX global run scoreboard players add #countX global 1
execute if score #dumbX global > #dumbZ global run scoreboard players add #countZ global 1