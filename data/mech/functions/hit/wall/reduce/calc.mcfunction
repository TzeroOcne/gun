# mech:hit/wall/reduce/calc

execute if entity @s[tag=countX] run function mech:hit/wall/reduce/calc_x
execute if entity @s[tag=countY] run function mech:hit/wall/reduce/calc_y
execute if entity @s[tag=countZ] run function mech:hit/wall/reduce/calc_z

execute if entity @s[tag=countX,tag=countY] run function mech:hit/wall/reduce/calc_xy
execute if entity @s[tag=countY,tag=countZ] run function mech:hit/wall/reduce/calc_yz
execute if entity @s[tag=countZ,tag=countX] run function mech:hit/wall/reduce/calc_zx

tag @e remove countX
tag @e remove countY
tag @e remove countZ

# summon armor_stand ~ ~2 ~ {NoGravity:1b}
# tellraw @a {"nbt" : "Tags", "entity" : "@s"}
execute at @s align xyz positioned ~0.5 ~0.5 ~0.5 run function mech:hit/wall/reduce/step_0

scoreboard players reset #countX global
scoreboard players reset #countY global
scoreboard players reset #countZ global