# mech:hit/wall/reduce/calc_xy

scoreboard players operation #dumbX global = #dltaX global
scoreboard players operation #dumbY global = #dltaY global

scoreboard players operation #dumbX global *= @s velcY
scoreboard players operation #dumbY global *= @s velcX

# execute if score #dumbX global > #dumbY global run tag @s remove countY
execute if score #dumbX global > #dumbY global run scoreboard players add #countY global 1
execute if score #dumbY global > #dumbX global run scoreboard players add #countX global 1