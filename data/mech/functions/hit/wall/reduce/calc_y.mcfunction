# mech:hit/wall/reduce/calc_y

scoreboard players operation #midY global = #lastY global
scoreboard players operation #midY global += #currY global
scoreboard players operation #midY global *= #500 global
scoreboard players add #midY global 500
scoreboard players operation #dltaY global = #midY global
execute store result score #lastY global run data get entity @s Pos[0] 1000
scoreboard players operation #dltaY global -= #lastY global
# scoreboard players operation #moveY global -= #lastY global
# execute if score #moveY global matches ..0 run scoreboard players operation #moveY global *= #-1 global
execute if score #dltaY global matches ..0 run scoreboard players operation #dltaY global *= #-1 global
scoreboard players set #countY global 0