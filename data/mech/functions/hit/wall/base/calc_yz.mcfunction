# mech:hit/wall/base/calc_yz

scoreboard players operation #dumbY global = #dltaY global
scoreboard players operation #dumbZ global = #dltaZ global

scoreboard players operation #dumbY global *= #moveZ global
scoreboard players operation #dumbZ global *= #moveY global

# execute if score #dumbY global > #dumbZ global run tag @s remove countZ
execute if score #dumbY global > #dumbZ global run scoreboard players add #countZ global 1
execute if score #dumbZ global > #dumbY global run scoreboard players add #countY global 1