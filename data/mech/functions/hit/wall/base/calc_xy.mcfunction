# mech:hit/wall/base/calc_xy

scoreboard players operation #dumbX global = #dltaX global
scoreboard players operation #dumbY global = #dltaY global

scoreboard players operation #dumbX global *= #moveY global
scoreboard players operation #dumbY global *= #moveX global

# execute if score #dumbX global > #dumbY global run tag @s remove countY
execute if score #dumbX global > #dumbY global run scoreboard players add #countY global 1
execute if score #dumbY global > #dumbX global run scoreboard players add #countX global 1