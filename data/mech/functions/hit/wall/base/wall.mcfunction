# mech:hit/wall/base/wall

summon armor_stand ~ ~ ~ {NoGravity:1b}
replaceitem entity @e[type=armor_stand,limit=1,sort=nearest] armor.head tnt
execute store result score #currX global run data get entity @s Pos[0]
execute store result score #currY global run data get entity @s Pos[1]
execute store result score #currZ global run data get entity @s Pos[2]

execute store result score #moveX global run data get entity @s Pos[0] 1000
execute store result score #moveY global run data get entity @s Pos[1] 1000
execute store result score #moveZ global run data get entity @s Pos[2] 1000

tp @s ^ ^ ^-1
execute at @s run summon armor_stand ^ ^ ^ {NoGravity:1b}
execute store result score #lastX global run data get entity @s Pos[0]
execute store result score #lastY global run data get entity @s Pos[1]
execute store result score #lastZ global run data get entity @s Pos[2]

execute unless score #currX global = #lastX global run tag @s add countX
execute unless score #currY global = #lastY global run tag @s add countY
execute unless score #currZ global = #lastZ global run tag @s add countZ
# tellraw @a [{"score" : {"name" : "#currX", "objective" : "global"}}, {"text" : " "}, {"score" : {"name" : "#lastX", "objective" : "global"}}]
scoreboard players set #countA global 0
execute if entity @e[tag=countX] run scoreboard players add #countA global 1
execute if entity @e[tag=countY] run scoreboard players add #countA global 1
execute if entity @e[tag=countZ] run scoreboard players add #countA global 1
# tellraw @a [{"score" : {"name" : "#countA", "objective" : "global"}}]

execute at @s if score #countA global matches 2.. run function mech:hit/wall/base/calc
# tellraw @a [{"nbt" : "Tags", "entity" : "@s"}]
execute if entity @s[tag=countX] store result entity @s Rotation[0] float 0.001 run data get entity @s Rotation[0] -1000
execute if entity @s[tag=countY] store result entity @s Rotation[1] float 0.001 run data get entity @s Rotation[1] -1000
execute if entity @s[tag=countZ] run tp @s ~ ~ ~ ~180 ~
execute if entity @s[tag=countZ] store result entity @s Rotation[0] float 0.001 run data get entity @s Rotation[0] -1000

# execute as @s at @s run tp @s ^ ^ ^1

tag @e remove countX
tag @e remove countY
tag @e remove countZ