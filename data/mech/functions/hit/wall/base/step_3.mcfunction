# mech:hit/wall/base/step_3

# say step 3
# summon armor_stand ~ ~ ~ {NoGravity:1b}
# replaceitem entity @e[type=armor_stand,limit=1,sort=nearest] armor.head diorite
execute unless block ~ ~ ~ #mech:translucent run tag @s add foundWall
execute if entity @s[tag=foundWall] if score #countX global matches 2 run tag @s add countX
execute if entity @s[tag=foundWall] if score #countY global matches 2 run tag @s add countY
execute if entity @s[tag=foundWall] if score #countZ global matches 2 run tag @s add countZ