# mech:hit/wall/base/calc_z

scoreboard players operation #midZ global = #lastZ global
scoreboard players operation #midZ global += #currZ global
scoreboard players operation #midZ global *= #500 global
scoreboard players add #midZ global 500
scoreboard players operation #dltaZ global = #midZ global
execute store result score #lastZ global run data get entity @s Pos[0] 1000
scoreboard players operation #dltaZ global -= #lastZ global
scoreboard players operation #moveZ global -= #lastZ global
execute if score #moveZ global matches ..0 run scoreboard players operation #moveZ global *= #-1 global
execute if score #dltaZ global matches ..0 run scoreboard players operation #dltaZ global *= #-1 global
scoreboard players set #countZ global 0