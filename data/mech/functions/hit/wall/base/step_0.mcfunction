# mech:hit/wall/base/step_0

# execute store result score #rottX global run data get entity @s Rotation[0] 1000
# execute store result score #rottY global run data get entity @s Rotation[1] 1000
# say step 0
# summon armor_stand ~ ~ ~ {NoGravity:1b}
# replaceitem entity @e[type=armor_stand,limit=1,sort=nearest] armor.head stone
# tellraw @a [{"score" : {"name" : "#countX", "objective" : "global"}}, {"text" : " "}, {"score" : {"name" : "#countY", "objective" : "global"}}, {"text" : " "}, {"score" : {"name" : "#countZ", "objective" : "global"}}]
execute if score #countX global matches 0 if entity @s[y_rotation=-180..0] positioned ~1 ~ ~ run function mech:hit/wall/base/step_1
execute if score #countX global matches 0 if entity @s[y_rotation=0..180] positioned ~-1 ~ ~ run function mech:hit/wall/base/step_1
# execute if score #countX global matches 0 if score #rottX global matches -180000..0 positioned ~1 ~ ~ run function mech:hit/wall/base/step_1
# execute if score #countX global matches 0 unless score #rottX global matches -180000..0 positioned ~-1 ~ ~ run function mech:hit/wall/base/step_1

execute if score #countY global matches 0 if entity @s[x_rotation=-90..0] positioned ~ ~1 ~ run function mech:hit/wall/base/step_1
execute if score #countY global matches 0 if entity @s[x_rotation=0..90] positioned ~ ~-1 ~ run function mech:hit/wall/base/step_1
# execute if score #countY global matches 0 if score #rottY global matches -90000..0 positioned ~ ~1 ~ run function mech:hit/wall/base/step_1
# execute if score #countY global matches 0 unless score #rottY global matches -90000..0 positioned ~ ~1 ~ run function mech:hit/wall/base/step_1

# execute if score #countZ global matches 0 if score #rottY global matches -90000..0 positioned ~ ~1 ~ run function mech:hit/wall/base/step_1
execute if score #countZ global matches 0 if entity @s[y_rotation=-90..90] positioned ~ ~ ~1 run function mech:hit/wall/base/step_1
execute if score #countZ global matches 0 if entity @s[y_rotation=90..-90] positioned ~ ~ ~-1 run function mech:hit/wall/base/step_1

tag @s remove foundWall