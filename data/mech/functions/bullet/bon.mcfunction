execute store result score @s currX run data get entity @s Motion[0] 1000
execute store result score @s currY run data get entity @s Motion[1] 1000
execute store result score @s currZ run data get entity @s Motion[2] 1000

execute if score @s[tag=!noSound] currX matches 0 unless score @s lastX matches -100..100 run tag @s add playSound
execute if score @s[tag=!noSound] currY matches 0 unless score @s lastY matches -100..100 run tag @s add playSound
execute if score @s[tag=!noSound] currZ matches 0 unless score @s lastZ matches -100..100 run tag @s add playSound
execute if entity @s[tag=playSound] run playsound minecraft:entity.slime.jump neutral @a ~ ~ ~ 0.5
tag @s remove playSound

execute if score @s currX matches 0 store result entity @s Motion[0] double -0.001 run scoreboard players get @s lastX
execute if score @s currY matches 0 store result entity @s Motion[1] double -0.001 run scoreboard players get @s lastY
execute if score @s currZ matches 0 store result entity @s Motion[2] double -0.001 run scoreboard players get @s lastZ

execute store result score @s lastX run data get entity @s Motion[0] 1000
execute store result score @s lastY run data get entity @s Motion[1] 1000
execute store result score @s lastZ run data get entity @s Motion[2] 1000