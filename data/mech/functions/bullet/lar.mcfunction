summon armor_stand ~ ~ ~ {NoGravity:1b, Small:1b}
particle crit ~ ~ ~ 0 0 0 0 1
function mech:hit/hit
scoreboard players add @s betweenTick 1
execute unless block ~ ~ ~ #mech:translucent as @s at @s run function mech:hit/wall/base/wall
execute as @s at @s run tp @s ^ ^ ^1
execute as @s at @s if block ~ ~ ~ #mech:translucent if score @s[tag=!killMe] betweenTick < @s moveVelocity run function mech:bullet/lar