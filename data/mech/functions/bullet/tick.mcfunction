# mech:bullet/tick

# Bon : Bouncy bullet
# Pod : Exploding bullet

scoreboard players set @e[tag=migBullet] betweenTick 0

# Exploding bullet
execute as @e[tag=tip,tag=pod] run data modify entity @s ArmorItems[].tag.OnTop set value 0b
execute as @e[tag=tipBullet,tag=pod] at @s run function mech:bullet/pod
execute as @e[tag=tip,tag=pod,nbt={ArmorItems:[{}, {}, {}, {tag:{OnTop:0b}}]}] at @s run function mech:hit/tip

# Bouncy bullet
execute as @e[tag=bouBullet,tag=bon] at @s run function mech:bullet/bon

# Reflecting bullet (Laser)
execute as @e[tag=farBullet,tag=far] at @s run function mech:bullet/far

execute as @e[tag=migBullet,tag=mig] at @s run function mech:bullet/mig

# execute as @e[tag=bullet,tag=lar] at @s run function mech:bullet/lar
# execute as @e[tag=bullet,tag=par] at @s run function mech:bullet/par